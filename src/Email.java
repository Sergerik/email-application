
import java.util.Scanner;


public class Email {
    private String email;
    private String firstName;
    private String lastName;
    private String password;
    private String department;
    private int mailboxCapacity = 500;
    private String alternativeEmail;
    private String companySuffix = "company.com";
    private int defaultPasswordLength = 10;

    public Email(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = setDepartment();

        this.password = randomPassword(defaultPasswordLength);
        System.out.println("Your password is: " + this.password);

        email = firstName.toLowerCase() + "." + lastName.toLowerCase() + "@" + department + "." + companySuffix;


    }

    private String setDepartment() {
        System.out.println("New worker: " + firstName + ". DEPARTMENT CODE: \n1.for Sales\n2.for Accounting\n3.for Development\n4.for None\nEnter department code: ");
        Scanner sc = new Scanner(System.in);
        int chose = sc.nextInt();
        if (chose == 1) {
            return "Sales";
        } else if (chose == 2) {
            return "Accounting";
        } else if (chose == 3) {
            return "Development";
        } else {
            return "";
        }
    }

    private String randomPassword(int length) {
        String passwordSet = "ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789!@#$%";
        char[] password = new char[length];
        for (int i = 0; i < password.length; i++) {
            int rand = (int) (Math.random() * passwordSet.length());
            password[i] = passwordSet.charAt(rand);

        }
        return new String(password);
    }

    public void setMailboxCapacity(int capacity) {
        this.mailboxCapacity = capacity;
    }

    public void setAlternativeEmail(String email) {
        this.alternativeEmail = email;
    }

    public void changePassword(String password) {
        this.password = password;
    }

    public int getMailboxCapacity() {
        return mailboxCapacity;
    }

    public String getAlternativeEmail() {
        return alternativeEmail;
    }

    public String getPassword() {
        return password;
    }

    public String showInfo() {
        return "DISPLAY NAME: " + firstName + " " + lastName + " " +
                "\nCOMPANY EMAIL: " + email + "\nMAILBOX CAPACITY IS: " + mailboxCapacity + "mb";
    }
}
